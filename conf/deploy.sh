#!/usr/bin/env bash

## configuration de wiki.org 
# configuration du serveur dns

# La commande ci-dessous crée un répertoire /etc/named sur la machine dwikiorg
himage dwikiorg mkdir -p /etc/named

# La commande ci-dessous copie le fichier se trouvant dans wiki.org/named.conf sur la machine dwikiorg dans le répertoire /etc/
hcp wiki.org/named.conf dwikiorg:/etc/.

# La commande ci-dessous copie tous les fichiers se trouvant dans wiki.org/* dans le répertoire  /etc/named sur la machine dwikiorg
hcp wiki.org/* dwikiorg:/etc/named/.

# la commande ci-dessou supprime (commande rm) le fichier /etc/named/named.conf se trouvant sur la machine dwikiorg
himage dwikiorg rm /etc/named/named.conf 


## configuration de iut.re 
# configuration du serveur dns


himage diutre mkdir -p /etc/named
hcp iut.re/named.conf diutre:/etc/.
hcp iut.re/* diutre:/etc/named/.
himage diutre rm /etc/named/named.conf 

## configuration de dorg
# configuration du serveur dns
himage dorg mkdir -p /etc/named
hcp dorg/named.conf dorg:/etc/.
hcp dorg/* dorg:/etc/named/.
himage dorg rm /etc/named/named.conf 

## configuration de dre
# configuration du serveur dns
himage dre mkdir -p /etc/named
hcp dre/named.conf dre:/etc/.
hcp dre/* dre:/etc/named/.
himage dre rm /etc/named/named.conf

## configuration de aRootServer
# configuration du serveur dns
himage aRootServer mkdir -p /etc/named
hcp aRootServer/named.conf aRootServer:/etc/.
hcp aRootServer/* aRootServer:/etc/named/.
himage aRootServer rm /etc/named/named.conf

## configuration de bRootServer
# configuration du serveur dns
himage bRootServer mkdir -p /etc/named
hcp bRootServer/named.conf bRootServer:/etc/.
hcp bRootServer/* bRootServer:/etc/named/.
himage bRootServer rm /etc/named/named.conf

## configuration de cRootServer
# configuration du serveur dns
himage cRootServer mkdir -p /etc/named
hcp cRootServer/named.conf cRootServer:/etc/.
hcp cRootServer/* cRootServer:/etc/named/.
himage cRootServer rm /etc/named/named.conf

## configuration de pc1
# resolv.conf
# Les fichiers ci-dessous ne sont pas présents
# du coup je commentes

#hcp pc1/resolv.conf pc1:/etc/.
